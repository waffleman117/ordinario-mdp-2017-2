﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Tarea
    {
        private int tareaId;
        private string nombreTarea;
        private int horaInicio;
        private int horaFin;
        private string estadoAnimo;
        private string materia;

        public int getTareaId()
        {
            return this.tareaId;
        }
        public void setTareaId(int ti)
        {
            this.tareaId=ti;
        }
        public string getNombreTarea()
        {
            return this.nombreTarea;
        }
        public void setNombreTarea(string nt)
        {
            this.nombreTarea = nt;
        }
        public int getHoraInicio()
        {
            return this.horaInicio;
        }
        public void setHoraInicio(int hi)
        {
            this.horaInicio=hi;
        }
        public int getHoraFin()
        {
            return this.horaFin;
        }
        public void setHoraFin(int hf)
        {
            this.horaFin=hf;
        }
        public string getEstadoAnimo()
        {
            return this.estadoAnimo;
        }
        public void setEstadoAnimo(string ea)
        {
            this.estadoAnimo=ea;
        }
        public string getMateria()
        {
            return this.materia;
        }
        public void setMateria(string m)
        {
            this.materia=m;
        }
    }
}
