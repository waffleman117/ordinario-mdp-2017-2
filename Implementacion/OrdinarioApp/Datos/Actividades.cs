﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Actividades
    {
        public List<Tarea> tareas { get; set; }
        //public List<Examen> Examenes;
        public Actividades()
        {
            this.tareas = new List<Tarea>();
        }
    }
}
