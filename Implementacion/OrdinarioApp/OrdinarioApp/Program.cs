﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
namespace OrdinarioApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Actividades acts = new Actividades();
            Tarea tarea = new Tarea();
            String opcion="";
            bool salir = false;
            int aux=0;

            Console.Write("Escribe tu nombre para comenzar: ");
            string nombre = Console.ReadLine();
 
            while(!salir)
            {
                Console.Clear();
                Console.WriteLine("\nBienvenido " + nombre + "!!");
                Console.WriteLine("Selecciona una opcion: \nA)Registrar una tarea");
                Console.WriteLine("\nS)Salir");
                if (acts.tareas.Any())
                {
                    Console.WriteLine("\nTareas registradas: ");
                    for (int i = 0; i < acts.tareas.Count; i++)
                    {
                        Console.WriteLine("|" + acts.tareas[i].getTareaId() + "| " + acts.tareas[i].getNombreTarea());
                    }
                }
                else
                    Console.WriteLine("No tienes ninguna tarea registrada aun!");
                Console.Write("\nopcion: ");
                opcion = Console.ReadLine();
                if(opcion.ToUpper()=="A")
                {
                    tarea.setTareaId(aux);
                    Console.Write("\n|"+aux+"| Escribe el nombre la tarea: ");
                    opcion = Console.ReadLine();
                    tarea.setNombreTarea(opcion);
                    bool isNumeric = false;
                    while (!isNumeric)
                    {
                        Console.Clear();
                        Console.Write("\nEscribe la hora de inicio: ");
                        opcion=Console.ReadLine();
                        Console.WriteLine("");
                        isNumeric = int.TryParse(opcion, out var hi);
                        if(isNumeric)
                        {
                            tarea.setHoraInicio(hi);
                            isNumeric = false;
                            while (!isNumeric)
                            {
                                Console.Clear();
                                Console.Write("\nEscribe la hora de finalizacion: ");
                                opcion = Console.ReadLine();
                                Console.WriteLine("");
                                isNumeric = int.TryParse(opcion, out var hf);
                                if (isNumeric)
                                {
                                    tarea.setHoraFin(hf);
                                }
                            }
                        }
                    }
                    Console.Write("\nDescribe tu estado de animo durante esa tarea: ");
                    opcion = Console.ReadLine();
                    Console.WriteLine("");
                    tarea.setEstadoAnimo(opcion);
                    Console.Write("\nEscribe la materia a la que pertenece esa tarea: ");
                    opcion = Console.ReadLine();
                    Console.WriteLine("");
                    tarea.setMateria(opcion);
                    acts.tareas.Add(tarea);
                    Console.WriteLine("Tarea registrada correctamente...");
                    //Console.ReadKey(true);
                }
                if (opcion.ToUpper() == "S")
                {
                    salir = true;
                }
            }
        }
    }
}
